package Sentencies;

import java.util.Scanner;

public class sentencies16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		String frase;
		char caracter;
		int countLA = 0;
		boolean flag = false;

		System.out.println("Digues una frase:");
		frase = sc.nextLine();

		for (int i = 0; i < frase.length(); i++) {
			caracter = Character.toLowerCase(frase.charAt(i));

			if (flag == true && caracter == 'a') {
				countLA++;
				flag = false;
			}
			if (caracter == 'l') {
				flag = true;
			} else {
				flag = false;
			}
		}

		if (countLA == 0) {
			System.out.println("No hi ha cap LA");
		}

		else {
			System.out.println("Hi ha " + countLA + " LA's");
		}
		sc.close();
	}
}
