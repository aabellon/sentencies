package Sentencies;

import java.util.Scanner;

public class sentencies14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		String cadena;
		cadena = sc.nextLine().toLowerCase();
		int a = 0;

		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == 'a')
				a++;
		}

		System.out.println("Total: Hi ha " + a + " lletres a");
		sc.close();
	}

}
// Amb el case tamb� es pot fer tenint en compte que les A, a poden portar accent.
// Amb System.out.println("posici� " + (i+1) + " es: " + cadena.charAt(i)); podem mostrar lletra per lletra les integrants de la cadena
