package Sentencies;

import java.util.Scanner;

public class sentencies13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		int nota = 0, contador = 0, suma = 0, excel = 0, not = 0, b� = 0, sufi = 0, insu = 0, md = 0;

		System.out.println("Digues les notes: ");
		while (nota != -1) {
			nota = sc.nextInt();
			suma = suma + nota;

			if (nota == 10 || nota == 9)
				excel++;
			else if (nota == 8 || nota == 7)
				not++;
			else if (nota == 6)
				b�++;
			else if (nota == 5)
				sufi++;
			else if (nota == 4 || nota == 3)
				insu++;
			else if (nota == 2 || nota == 1 || nota == 0)
				md++;

			contador++;
		}

		System.out.println("La mitjana aritm�tica �s: " + (suma / contador));
		System.out.println("N�mero d'excel�lents: " + excel);
		System.out.println("N�mero de notables: " + not);
		System.out.println("N�mero de b�'s: " + b�);
		System.out.println("N�mero de suficients: " + sufi);
		System.out.println("N�mero d'insuficients: " + insu);
		System.out.println("N�mero de molt deficients: " + md);

		sc.close();
	}

}
