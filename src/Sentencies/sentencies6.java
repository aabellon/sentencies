package Sentencies;

import java.util.Scanner;

public class sentencies6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		int num, neg, pos, zero;
		neg = 0;
		pos = 0;
		zero = 0;

		for (int i = 0; i < 10; i++) {
			System.out.println("Valor " + (i + 1));
			num = sc.nextInt();

			if (num < 0) {
				neg++;
			}
			if (num > 0) {
				pos++;
			} else {
				zero++;
			}
		}

		System.out.println("Hi ha " + neg + " n�meros negatius");
		System.out.println("Hi ha " + pos + " n�meros positius");
		System.out.println("Hi ha " + zero + " n�meros zeros");
		sc.close();

	}

}
