package Sentencies;

import java.util.Scanner;

public class sentencies8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int num;
		long factorial = 1;

		System.out.println("Digues el n�mero:");
		num = sc.nextInt();

		for (int i = num; i > 0; i--) {
			factorial = factorial * i;
		}

		System.out.println("El factorial de " + num + " �s de: " + factorial);
		sc.close();
	}
}
